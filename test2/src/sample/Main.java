package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
//		File folder = new File ("DIR");
//		File folder1 = new File ("DIR1");
//		File file = new File ("C:\\0 --- USER ---\\1 DOCUMENTS\\1 My documents\\1 ProgAcademy\\5 JAVA OOP\\Progects\\test2\\DIR\\text.txt");
//		File file1 = new File ("C:\\0 --- USER ---\\1 DOCUMENTS\\1 My documents\\1 ProgAcademy\\5 JAVA OOP\\Progects\\test2\\DIR1\\text1.txt");
//		File workFolder = new File ("..");
//		try {
//			System.out.println(folder.mkdirs());
//			System.out.println(file.createNewFile());
//
//			System.out.println(folder1.mkdirs());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(file.getAbsolutePath());
//		System.out.println(file.getName());
//		System.out.println(file.isDirectory());
//		System.out.println(file.isFile());
//		System.out.println(file.exists());
//		System.out.println(workFolder.getName());
//		System.out.println(workFolder.listFiles());
//		System.out.println(file.renameTo(file1));
		
		
//		File workFolder = new File (".");
//		System.out.println(workFolder.getAbsolutePath());
//		File workFolder2 = new File ("..");
//		System.out.println(workFolder2.getAbsolutePath());
//		File[] files = workFolder2.listFiles();
//		for (int j = 0; j < files.length; j++) {
//			System.out.println(files[j] + System.lineSeparator());
//		}
		
		File fileTest = new File("test.txt");
		
		try (PrintWriter pw = new PrintWriter (fileTest)){
			pw.println("Hello world");
			pw.println("Hello Java");
			pw.println("Hello Hello Hello");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (Scanner sc = new Scanner (fileTest)){
			String rez = "";
			for(;sc.hasNextLine();) {
				rez += sc.nextLine() + System.lineSeparator() ; 
			}
			System.out.println(rez);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		
	}

}
