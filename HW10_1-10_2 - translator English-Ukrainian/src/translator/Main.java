package translator;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File engText = new File("English.in.txt");
		File uaText = new File("Ukrainian.out.txt");
		
		TranslationFromEnglish tfe = new TranslationFromEnglish();
		tfe.addWordToVocabulary();
		tfe.translationFromEnglishAndSaveIntoFile(engText, uaText); 
		
//		Map <String, String> vocabulary = new HashMap<>();
//		vocabulary.put("love", "люблю");
//		vocabulary.put("you", "тебя");
//		vocabulary.put("I", "Я");
//		vocabulary.put("Help", "Помоги");
//		vocabulary.put("me", "мне");
//		
//		translationFromEnglish.translationFromEnglishAndSaveIntoFile(engText, uaText, vocabulary); 
		
		
		
	}

}
