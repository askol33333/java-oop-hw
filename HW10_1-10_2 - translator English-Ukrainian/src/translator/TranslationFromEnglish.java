package translator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class TranslationFromEnglish {

	private Map<String, String> hm = new HashMap<>();

	public TranslationFromEnglish() {
		super();
	}

	public TranslationFromEnglish(Map<String, String> hm) {
		super();
		this.hm = hm;
	}

	public Map<String, String> getHm() {
		return hm;
	}

	public void setHm(Map<String, String> hm) {
		this.hm = hm;
	}

	public void vocabularyActivate() {
		hm.put("love", "кохаю");
		hm.put("you", "тебе");
		hm.put("I", "Я");
		hm.put("Help", "Допоможи");
		hm.put("me", "мені");
	}
	
	public void addWordToVocabulary() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Дополняем словарь.");
		System.out.println("Введите слово на английском языке.");
		String engWord = sc.nextLine();
		System.out.println("Дополняем словарь.");
		System.out.println("Введите перевод ранее введенного слова на украинскй язык.");
		String uaWord = sc.nextLine();
		hm.put(engWord, uaWord);
		
		//File vocabulary = new File("vocabulary.csv");
		File vocabulary = new File("vocabulary.txt");
		vocabularyActivate();
		try (PrintWriter pw = new PrintWriter(vocabulary)) {
			for (String key : hm.keySet()) {
		//		pw.println(key + ";" + hm.get(key)+ ";");	
				pw.println(key + "=" + hm.get(key));	
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<String> loadStringFromFile(File file) {
		List<String> textLines = new ArrayList<>();
		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				textLines.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return textLines;
	}

//	public static List<String> translateText(List <String> textLines, Map <String,String> hm) {
//		List<String> textLinesUa = new ArrayList<>();
//		for (String text : textLines) {
//			String[] wordsArray = text.split("[ ]");
//			String translateText = "";
//				for (int i = 0; i < wordsArray.length; i++) {
//					if(hm.containsKey(wordsArray[i])) {
//						translateText += hm.get(wordsArray[i]) + " "; 
//					}
//				}
//				textLinesUa.add(translateText);
//		}		
//	return textLinesUa;	
//	}

	public List<String> translateText(List <String> textLines) {
		vocabularyActivate();
		List<String> textLinesUa = new ArrayList<>();
		for (String text : textLines) {
			String[] wordsArray = text.split("[ ]");
			String translateText = "";
				for (int i = 0; i < wordsArray.length; i++) {
					if(hm.containsKey(wordsArray[i])) {
						translateText += hm.get(wordsArray[i]) + " "; 
					}
				}
				textLinesUa.add(translateText);
		}		
	return textLinesUa;	
	}
	
	public static void saveTranslationStringIntoFile(File file, List <String> textLines) {
		try (PrintWriter pw = new PrintWriter(file)) {
			for (String text : textLines) {
				pw.println(text);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public static void translationFromEnglishAndSaveIntoFile(File fileIn, File fileOut, Map<String,String> hm) {
//		saveTranslationStringIntoFile(fileOut, translateText(loadStringFromFile(fileIn), hm));
//	}
	
	public void translationFromEnglishAndSaveIntoFile(File fileIn, File fileOut) {
		saveTranslationStringIntoFile(fileOut, translateText(loadStringFromFile(fileIn)));
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(hm);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslationFromEnglish other = (TranslationFromEnglish) obj;
		return Objects.equals(hm, other.hm);
	}

	@Override
	public String toString() {
		return "translationFromEnglish [hm=" + hm + "]";
	}

	
}
