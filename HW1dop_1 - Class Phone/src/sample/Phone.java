package sample;

public class Phone {
	
	private int numerPhone;
	private Network nw;

	public Phone(int numerPhone) {
		super();
		this.numerPhone = numerPhone;
		
	}

	public Phone() {
		
	}

	public int getNumerPhone() {
		return numerPhone;
	}

	public void setNumerPhone(int numerPhone) {
		this.numerPhone = numerPhone;
	}

	public void registration(Network network) {
		nw = network;
	network.registrationNumber(this.numerPhone);
		System.out.println("Регистрация номера - "+ this.numerPhone +" - выполнена.");
	}

	public void outCall(int inNumber) {
		if (this.nw == null) {
			System.out.println("Ваш номер в сети не зарегистрирован!");

		} else {
			for (int i = 0; i < nw.getRegNumbers().length; i++) {
				if (inNumber == nw.getRegNumbers()[i]) {
					inCall(nw.getRegNumbers()[i]);
					break;
				}
				if (inNumber == this.numerPhone) {
					System.out.println("Номер занят. Перезвоните позже.");
					break;
				}
				if (i == nw.getRegNumbers().length - 1) {
					System.out.println("Номер на который вы звоните -"+ inNumber +"-, в сети не зарегистрирован!");
				}
			}
		}

	}

	public void inCall(int inNumber) {
		System.out.println("На номер - " + inNumber + " - поступил звонок с номера - " + this.numerPhone + " -.");
	}
	
	@Override
	public String toString() {
		return "Phone [numerPhone=" + numerPhone + ", nw=" + nw + "]";
	}

}
