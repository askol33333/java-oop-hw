package sample;

import java.util.Arrays;

public class Network {
	private int [] regNumbers = new int[0];

	public Network(int[] regNumbers) {
		super();
		this.regNumbers = regNumbers;
	}
	
	public Network() {
		
	}

	public int[] getRegNumbers() {
		return regNumbers;
	}

	public void setRegNumbers(int[] regNumbers) {
		this.regNumbers = regNumbers;
	}

	public void registrationNumber (int regNumber) {
		regNumbers = Arrays.copyOf(regNumbers, regNumbers.length + 1);
		regNumbers[regNumbers.length - 1] = regNumber;
	}
	
	@Override
	public String toString() {
		return "Network [regNumbers=" + Arrays.toString(regNumbers) + "]";
	}
		
}


