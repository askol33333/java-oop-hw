package sample;

public class Main {

	public static void main(String[] args) {
	
		Network nw = new Network();

		Phone number1 = new Phone(12345);
		number1.registration(nw);

		Phone number2 = new Phone(56789);
		number2.registration(nw);

		Phone number3 = new Phone(13579);

		System.out.println(number1);
		System.out.println(number2);
		System.out.println(number3);
		
		number1.outCall(56789);
		number2.outCall(56789);
		number2.outCall(13579);
		number3.outCall(56789);
		number3.outCall(13579);
	}

}
