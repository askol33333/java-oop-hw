package sample;

import java.lang.reflect.Field;
import java.util.Scanner;

public class Main {

  public static Cat cat;

  public static void main(String[] args) {
    Cat cat1 = new Cat("Luska", 8);
    Cat cat2 = new Cat("Luska", 8);

    System.out.println(cat1 == cat2);
    System.out.println(cat1.equals(cat2));
    System.out.println(cat1.hashCode());
    System.out.println(cat2.hashCode());
    System.out.println("-------------------------------");

    try {
      Cat cat3 = cat1.clone();
      System.out.println(cat1 == cat3);
      System.out.println(cat1.getClass());
      System.out.println(cat3.getClass());
      System.out.println(cat3.equals(cat1));

    } catch (CloneNotSupportedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println("-------------------------------");

    Class catClass = Cat.class;

    Field[] fields = catClass.getDeclaredFields();
    for (int i = 0; i < fields.length; i++) {
      System.out.println(fields[i]);
    }
    System.out.println("-------------------------------");

    try {
      Field catAge = catClass.getDeclaredField("age");
      catAge.setAccessible(true);
      catAge.setInt(cat1, 100);
    } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println(cat1);
    
  }

}