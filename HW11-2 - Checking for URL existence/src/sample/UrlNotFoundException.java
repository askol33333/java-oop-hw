package sample;

public class UrlNotFoundException extends Exception {

	public UrlNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UrlNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UrlNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UrlNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
