package sample;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class NetworkService {

	public static void testForUrl(File file) {
		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				String testHeaders = "";
				String testUrl = sc.nextLine();
				Map<String, List<String>> headers = getHeader(testUrl);
				testHeaders = getStringTestHeader(headers);				
				if (testHeaders == "") {		
					throw new UrlNotFoundException("URL: " + testUrl + ", not exsist.");
				} else {
					System.out.println("URL: " + testUrl + ", exsist.");
				} 		
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static Map<String, List<String>> getHeader(String sepc) throws IOException {
		URL url = new URL(sepc);
		URLConnection connection = url.openConnection();
		return connection.getHeaderFields();
	}
	
	public static String getStringTestHeader(Map<String, List<String>> map) {
		String testHeader = "";
		for (String k : map.keySet()) {
			testHeader += k + " " + map.get(k);	
		} 
		return testHeader;
	}
	

}
