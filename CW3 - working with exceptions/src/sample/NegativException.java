package sample;

public class NegativException extends Exception  {

	public NegativException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NegativException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NegativException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NegativException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
