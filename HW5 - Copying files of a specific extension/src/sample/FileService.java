package sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileService {

	public static long copyFile(File fileIn, File fileOut) throws IOException {
		try (InputStream is = new FileInputStream(fileIn); OutputStream os = new FileOutputStream(fileOut)) {
			return is.transferTo(os);
		}
	}

	public static void copyAllFiles(File folderIn, File folderOut) throws IOException {
		File[] files = folderIn.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				File fileOut = new File(folderOut, files[i].getName());
				System.out.println(copyFile(files[i], fileOut));
			}
		}
	}

	public static void copyAllFilesSpecificExtention(File folderIn, File folderOut, String extension) throws IOException {
		File[] files = folderIn.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				char[] tempArray = files[i].getName().toCharArray();
				if (files[i].getName().substring(tempArray.length - 3, tempArray.length).equals(extension)) {
					File fileOut = new File(folderOut, files[i].getName());
					System.out.println(copyFile(files[i], fileOut));
				}
			}
		}
	}

}