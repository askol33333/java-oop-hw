package sample;

import java.util.Arrays;

public class Order {
	private final Product[] products;

	public Order() {
		super();
		products = new Product[100];
	}

	public Product[] getProducts() {
		return products;
	}

	public void addProduct(Product product) {
		for (int i = 0; i < products.length; i++) {
			if (products[i] == null) {
				products[i] = product;
				return;
			}
		}
	}

	public Product searchProductById(int id) throws ProductNotFoundException {
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				if (products[i].getId() == id) {
					return products[i];
				}
			}
		}
		throw new ProductNotFoundException();
	}

	public boolean removeProductById(int id) {
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				if (products[i].getId() == id) {
					products[i] = null;
					return true;
				}
			}
		}
		return false;	
	}

	public int calculateTotalSum() {
		int sum =0;
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				sum += products[i].getPrice();
				
			}
		}
		return sum;
	}

	@Override
	public String toString() {
		String result="";
		int count = 0; 
		for (int i = 0; i < products.length; i++) {
			if(products[i] != null) {
				count+=1;
				result += count + "." + products[i] + "." + System.lineSeparator();
			}
		}
		return "Order (" + count + " products):" + System.lineSeparator() + result + "TOTAL SUM = " + calculateTotalSum() + ".";
	}

}
 