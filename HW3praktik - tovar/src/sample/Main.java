package sample;

public class Main {

	public static void main(String[] args) {

		Product product1 = new Product("Table", 20, 0);
		Product product2 = new Product("Chair", 10, 1);
		Product product3 = new Product("Bed", 30, 2);
		Product product4 = new Product("Scarf", 25, 3);
		Product product5 = new Product("Sofa", 35, 4);
		
		Order order1 = new Order();
		
		order1.addProduct(product1);
		order1.addProduct(product5);
		order1.addProduct(product2);
		order1.addProduct(product4);
		
		System.out.println(order1);
		
		try {
			Product product6 = order1.searchProductById(4);
			System.out.println(product6);
		} catch (ProductNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println();
		
		order1.removeProductById(1);
		
		System.out.println(order1);
	}
	
	
	
}
