package sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

//		Map<String, String> map = new HashMap<>();
//
//		map.put("j", "Java");
//		map.put("t", "the");
//		map.put("b", "best");
//
//		System.out.println(map);
//
//		String res = map.get("a");
//
//		System.out.println(res);
//
////    map.put("j", "JS");
////    
////    System.out.println(map);
//
//		for (String k : map.keySet()) {
//			System.out.println(k + " " + map.get(k));
//		}
//
//		map.remove("b");
//
//		System.out.println(map);

//	      List<Integer> list = new ArrayList<>();
//	      Random rn = new Random();
//	      for (int i = 0; i < 15; i++) {
//	        list.add(rn.nextInt(10));
//	      }
//	      System.out.println(list);
//	      
//	      Map<Integer,Integer> stat = new HashMap<>();
//	      int counter = 1;
//	      for (Integer number : list) {
//	    	  if(stat.containsKey(number)) {
//	    		  counter = stat.get(number);
//	    		  stat.put(number, counter+1);
//	    	  }
//	    	  else {
//	    		  stat.put(number, 1);
//	    	  }
//	      }
//	      
//	      System.out.println(stat);

		List<Integer> list = new ArrayList<>();
		Random rn = new Random();
		for (int i = 0; i < 15; i++) {
			list.add(rn.nextInt(10));
		}
		System.out.println(list);

		Map<Integer, Integer> stat = new HashMap<>();

		for (Integer number : list) {

			Integer n = stat.get(number);
			if (n == null) {
				stat.put(number, 1);
			} else {
				stat.put(number, n+1);
			}
			
		}

		System.out.println(stat);

	}

}
