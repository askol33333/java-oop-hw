package group.students;

public class GroupDuplicatStudentsException extends Exception {

	public GroupDuplicatStudentsException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GroupDuplicatStudentsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GroupDuplicatStudentsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GroupDuplicatStudentsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
