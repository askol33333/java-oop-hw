package group.students;

public class WrongGroupException extends Exception {

	public WrongGroupException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WrongGroupException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WrongGroupException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WrongGroupException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
