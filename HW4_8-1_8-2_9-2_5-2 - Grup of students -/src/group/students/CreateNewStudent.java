package group.students;

import java.util.Scanner;

public class CreateNewStudent {
	Scanner sc = new Scanner(System.in);

	private Student newStudent = new Student(null, null, null, 0, null);;

	public CreateNewStudent(Student newStudent) {
		super();
		this.newStudent = newStudent;
	}

	public CreateNewStudent() {
		super();
	}

	public Student getNewStudent() {
		return newStudent;
	}

	public void setNewStudent(Student newStudent) {
		this.newStudent = newStudent;
	}

	public String getNameNS() {
		System.out.println("Input name of the new student.");
		String nameNS = sc.nextLine();
		return nameNS;
	}

	public String getLastNameNS() {
		System.out.println("Input student lastName.");
		String lastNameNS = sc.nextLine();
		return lastNameNS;
	}

	public String getGenderNS() {
		System.out.println("Input student gender (MALE / FEMALE).");
		String genderNS = sc.nextLine();
		return genderNS;
	}

	public String getGroupNameNS() {
		System.out.println("Input group's name of the new student.");
		String groupNameNS = sc.nextLine();
		return groupNameNS;
	}

	public Student CreateNewStudentIn() throws GenderNotFoundException{
		newStudent.setName(getNameNS());
		newStudent.setLastName(getLastNameNS());
		
		String temp = getGenderNS();
		if (temp.equals("MALE")) {
			newStudent.setGender(Gender.MALE);
		} else if (temp.equals("FEMALE")) {
			newStudent.setGender(Gender.FEMALE);
		} else {
			throw new GenderNotFoundException ("You have provided incorrect information.Input the gender (MALE / FEMALE).");
		}

		newStudent.setGroupName(getGroupNameNS());
		System.out.println(System.lineSeparator() + "New student created: " + newStudent);
		return newStudent;
	}

	public void addNewStudentToGroup(Group group) throws WrongGroupException {
		if (group.getGroupName().equals(newStudent.getGroupName())){
			try {
				group.addStudent(newStudent);
			} catch (GroupOverflowException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new WrongGroupException("You are mistaken. This student must be enrolled in the group:" + newStudent.getGroupName());
		}	
	}
	
	@Override
	public String toString() {
		return "" + newStudent;
	}

}
