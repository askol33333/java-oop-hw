package group.students;

public class GenderNotFoundException extends Exception {

	public GenderNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GenderNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GenderNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GenderNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
