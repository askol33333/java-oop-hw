package group.students;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class GroupFileStorage {

	public static void saveGroupToCSV(Group gr) {
		File file = new File("C:\\0 --- USER ---\\1 DOCUMENTS\\1 My documents\\1 ProgAcademy\\5 JAVA OOP\\Progects\\HW4_8-1_8-2_9-2_5-2 - Grup of students -\\BAZE\\"+ gr.getGroupName() + ".csv");
		try (PrintWriter pw = new PrintWriter(file)) {
			for (Student student : gr.getStudents()) {
				pw.println(student.getName() + ";" + student.getLastName() + ";" + student.getGender() + ";"
						+ student.getId() + ";");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gr.getStudents().clear();
	}

	public static Group loadGroupFromCSV(File file) {
		String fileName = file.getName();
		String grName = fileName.substring(0, fileName.length() - 4);
		Group gr = new Group(grName);
		try (Scanner sc = new Scanner(file)) {
			String rez = "";
			for (; sc.hasNextLine();) {
				rez = sc.nextLine();
				Student student = new Student("", "", null);
				String[] rezArray = rez.split("[;]");
				if (rezArray[2].equals("MALE")) {
					student = new Student(rezArray[0], rezArray[1], Gender.MALE);
				} else {
					student = new Student(rezArray[0], rezArray[1], Gender.FEMALE);
				}
				gr.addStudent(student);
				student.setId(Integer.parseInt(rezArray[3]));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return gr;
	}

	public static File findFileByGroupName(String groupName, File workFolder) throws fileNotFoundException {
		String fileName = groupName + ".csv";
		File[] listFiles = workFolder.listFiles();
		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].getName().equals(fileName)) {
				return listFiles[i];
			}
		}
			throw new fileNotFoundException("File: " + groupName + ", not found.");
		
	}

}
