package group.students;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import group.students.GroupOverflowException;
import group.students.Student;
import group.students.StudentNotFoundException;

public class Group {
	private String groupName;
	// private Student[] students = new Student[10];
	private List<Student> students = new ArrayList<>();

	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}

	public Group() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

//	public void addStudent(Student student) throws GroupOverflowException {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] == null) {
//				students[i] = student;
//				students[i].setGroupName(groupName);
//				students[i].setId(i + 1);
//				return;
//			}
//		}
//		throw new GroupOverflowException("The group is full. There are no more places.");
//	}

	public void addStudent(Student student) throws GroupOverflowException {
		if (students.size() < 10) {
			students.add(student);
			students.get(students.size() - 1).setGroupName(groupName);
			students.get(students.size() - 1).setId(students.size());
			return;
		}
		throw new GroupOverflowException("The group is full. There are no more places.");
	}
//	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] != null) {
//				if (students[i].getLastName().equals(lastName)) {
//					return students[i];
//				}
//			}
//		}
//		throw new StudentNotFoundException("There is no student with the last name: " + lastName.toUpperCase()
//				+ " in the group: " + groupName.toUpperCase() + ".");
//	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (Student student : students) {
			if (student.getLastName().equals(lastName)) {
				return student;
			}
		}
		throw new StudentNotFoundException("There is no student with the last name: " + lastName.toUpperCase()
				+ " in the group: " + groupName.toUpperCase() + ".");
	}

//	public boolean removeStudentById(int id) {
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] != null) {
//				if (students[i].getId() == id) {
//					students[i] = null;
//					return true;
//				}
//			}
//		}
//		return false;
//	}

	public boolean removeStudentById(int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				student = null;
				return true;
			}
		}
		return false;
	}

//	public void sortStudentsByLastName() {
//		Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));	
//	}

	public void sortStudentsByLastName() {
		Collections.sort(students, new StudentLastNameComparator());
	}
//	public void equalsStudentAtGroup() throws GroupDuplicatStudentsException {
//	for (int i = 0; i < students.length - 1; i++) {
//		for (int j = 0; j < students.length; j++) {
//			if (i != j && j != i - 1 && students[i] != null && students[j] != null) {
//				if (students[i].equals(students[j])) {
//					throw new GroupDuplicatStudentsException("This student is already in group:" + groupName + ".");
//				}
//			}
//		}
//	}
//}
	
	public void equalsStudentAtGroup() throws GroupDuplicatStudentsException {
		for (int i = 0; i < students.size() - 1; i++) {
			for (int j = 0; j < students.size(); j++) {
				if (i != j && j != i - 1) {
					if (students.get(i).equals(students.get(j))) {
						throw new GroupDuplicatStudentsException("This student is already in group:" + groupName + ".");
					}
				}
			}
		}
	}
	
	
	
//	@Override
//	public String toString() {
//		String group = "Group: " + groupName.toUpperCase() + "." + System.lineSeparator();
//		int count = 0;
//		for (int i = 0; i < students.length; i++) {
//			if (students[i] != null) {
//				count += 1;
//				group += i + 1 + "." + students[i] + "." + System.lineSeparator();
//			}
//		}
//		return group + "Total group of " + count + " students. ";
//	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + Arrays.hashCode(students);
//		result = prime * result + Objects.hash(groupName);
//		return result;
//	}

	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}

//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Group other = (Group) obj;
//		return Objects.equals(groupName, other.groupName) && Arrays.equals(students, other.students);
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}

	@Override
	public String toString() {
		String group = "Group: " + groupName.toUpperCase() + "." + System.lineSeparator();
		for (int i = 0; i < students.size(); i++) {
			group += i + 1 + "." + students.get(i) + "." + System.lineSeparator();
		}
		return group + "Total group of " + students.size() + " students. ";
	}

	
}