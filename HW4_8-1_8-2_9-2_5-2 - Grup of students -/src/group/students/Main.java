package group.students;

import java.io.File;

import group.students.Gender;
import group.students.Group;
import group.students.GroupOverflowException;
import group.students.Student;
import group.students.StudentNotFoundException;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student student1 = new Student("Oleg", "Podolyan", Gender.MALE);
		Student student2 = new Student("Irina", "Belova", Gender.FEMALE);
		Student student3 = new Student("Maksim", "Spirkin", Gender.MALE);
		Student student4 = new Student("Maksim", "Nagnibeda", Gender.MALE);
		Student student5 = new Student("Olga", "Shevchenko", Gender.FEMALE);
		Student student6 = new Student("Oleksandr", "Pozhidaev", Gender.MALE);
		Student student7 = new Student("Igor", "Pruhodiko", Gender.MALE);
		Student student8 = new Student("Oleksandr", "Ugranovskiy", Gender.MALE);
		Student student9 = new Student("Mariya", "Figova", Gender.FEMALE);
		Student student10 = new Student("Volodumir", "Feschenko", Gender.MALE);
		Student student11 = new Student("Vasya", "Pyatochkin", Gender.MALE);

		// List<Student> students = new ArrayList<>();

		Group group1 = new Group("JAVA-OOP-061022");
		Group group2 = new Group("JAVA-OOP-101122");
		// Group group2 = new Group("JAVA-START-071022");
		try {

			group1.addStudent(student1);
//			group1.addStudent(student2);
//			group1.addStudent(student3);
//			group1.addStudent(student4);
//			group1.addStudent(student5);
//			group1.addStudent(student6);
//			group1.addStudent(student7);
//			group1.addStudent(student8);
//			group1.addStudent(student9);
//			group1.addStudent(student10);

//			System.out.println(group1);
//			System.out.println();
//			group1.addStudent(student11);

			System.out.println(group1.searchStudentByLastName("Podolyan"));
//			System.out.println(group1.searchStudentByLastName("Dorn")); 

//			System.out.println(group1.removeStudentById(5));
//			group1.addStudent(student11);
//			System.out.println();	
//			System.out.println(group1);	

//			group1.sortStudentsByLastName();
//			System.out.println(group1);
//			System.out.println();

//			student12.CreateNewStudentIn();
//			student12.addNewStudentToGroup(group1);
//			student12.addNewStudentToGroup(group2);
//			System.out.println();
//			System.out.println(group2);
			group1.addStudent(student2);
			group1.addStudent(student3);
			group1.addStudent(student4);
//			group1.addStudent(student7);
			group1.addStudent(student5);
			group1.addStudent(student6);
			group1.addStudent(student7);
//			System.out.println(group1);

			group2.addStudent(student1);
			group2.addStudent(student2);
			group2.addStudent(student3);
//			System.out.println(group2);

			group1.equalsStudentAtGroup();

			group1.sortStudentsByLastName();
//			System.out.println(group1);

			File bazeOfGroup = new File("BAZE");
			bazeOfGroup.mkdirs();

			GroupFileStorage.saveGroupToCSV(group1);
			System.out.println(group1);

			File fileGroup1 = new File("C:\\0 --- USER ---\\1 DOCUMENTS\\1 My documents\\1 ProgAcademy\\5 JAVA OOP\\Progects\\HW4_8-1_8-2_9-2_5-2 - Grup of students -\\BAZE\\JAVA-OOP-061022.csv");
			Group newGroup1 = GroupFileStorage.loadGroupFromCSV(fileGroup1);
			System.out.println(newGroup1);

			GroupFileStorage.saveGroupToCSV(group2);
			System.out.println(group2);

			File fileGroup2;
			fileGroup2 = GroupFileStorage.findFileByGroupName("JAVA-OOP-101122", bazeOfGroup);
			Group newGroup2 = GroupFileStorage.loadGroupFromCSV(fileGroup2);
			System.out.println(newGroup2);
			
		} catch (GroupOverflowException | StudentNotFoundException | GroupDuplicatStudentsException |fileNotFoundException e) {
// Исключения при добавлении нового студента с клавиатуры == | GenderNotFoundException | WrongGroupException
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

}
