package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

public class NetworkService {

	public static String getStringFromURL(String spec, String code) throws IOException {
		URL url = new URL(spec);
		URLConnection connection = url.openConnection();
		String text = "";
		try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), code))) {
			for (;;) {
				String temp = br.readLine();
				if (temp == null) {
					break;
				}
				text += temp + System.lineSeparator();
			}
			return text;
		}
	}

	public static void getLinksWithUrlToFile(String text, File folder) throws IOException {
		String textForFile = "";
		String x = "";
		String tempText = "";
		int e = 1;
		for (;;) {
			int s = text.indexOf("h");
			if (s == -1) {
				break;	
			}
			String textStartWithH = text.substring(s);
			String testText = text.substring(s, s + 5);
			if (testText.equals("href=")) {
				tempText = textStartWithH.substring(6);
				x = textStartWithH.substring(5,6);
				e = tempText.indexOf(x);
				textForFile += tempText.substring(0, e) + System.lineSeparator();			
			}
			text = textStartWithH.substring(1);
		}
		File file = new File(folder, "url.txt");
		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(textForFile);
		}
	}

}
