package sample;

public class Product {
	private double price;
	private String description;
	private double volume;

	public Product(double price, String description, double volume) {
		super();
		this.price = price;
		this.description = description;
		this.volume = volume;
	}

	public Product() {
		
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public String toString() {
		return "Product [price=" + price + ", description=" + description + ", weight=" + volume + "]";
	}
	
}
