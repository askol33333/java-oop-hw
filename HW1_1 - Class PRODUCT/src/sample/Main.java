package sample;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
		Product product1 = new Product(45.50,"bucket", 10.0);
		Product product2 = new Product(110.20,"barrel", 50.0);
		Product product3 = new Product();
		
		product3.setPrice(12.0);
		product3.setDescription("jar");
		product3.setVolume(0.5);
		
		System.out.println(product1);
		System.out.println(product2);
		System.out.println(product3);
		
		System.out.println(product2.getPrice());
		System.out.println(product2.getDescription());
		System.out.println(product2.getVolume());
	}
}
