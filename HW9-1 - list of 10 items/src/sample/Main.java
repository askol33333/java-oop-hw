package sample;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		listModification();
	}

	public static List createListOfTenElements() {
		List<Integer> listOfTenElements = new ArrayList<>();
		for (int i = 1; i <= 10; i++) {
			listOfTenElements.add(i);
		}
		return listOfTenElements;
	}

	public static void listModification() {
		List<Integer> list = createListOfTenElements();
		System.out.println("Список из 10 элементов: " + list);
		list.remove(0);
		list.remove(0);
		list.remove(list.size() - 1);
		System.out.println("Модифицированный список: " + list);
	}
}