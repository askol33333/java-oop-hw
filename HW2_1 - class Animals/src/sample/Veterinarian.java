package sample;

public class Veterinarian {
	private String name;

	public Veterinarian(String name) {
		super();
		this.name = name;
	}
	
	public Veterinarian() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void treatment(Animal animal) {
		
		System.out.println("My name is " + this.name +". I am veterinarian. I am healing: " + animal);
	}

	@Override
	public String toString() {
		return "Veterinarian [name=" + name + "]";
	}
	
	
}
