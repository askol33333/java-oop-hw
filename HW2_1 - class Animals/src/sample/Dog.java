package sample;

public class Dog extends Animal{
	private String name;
	
	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Dog() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getVoice() {
		return "rrr gav gav";
	}
	
	public void eat() {
		System.out.println("plyam plyam plyam");
	}
	
	public void sleep() {
		System.out.println("hrrr hrrrr hr");
	}

	@Override
	public String toString() {
		return "Dog [name=" + name + ", " + super.toString()+"]";
	}
	
}
