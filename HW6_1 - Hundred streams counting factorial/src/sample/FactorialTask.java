package sample;

import java.math.BigInteger;

public class FactorialTask implements Runnable {
	private int n;

	public FactorialTask(int n) {
		super();
		this.n = n;
	}

	public FactorialTask() {
		super();
	}

	public int getN() {
		return n;
	}

	public BigInteger calculateFactorial(int number) {
		BigInteger fact = BigInteger.ONE;
		for (int i = 1; i <= number; i++) {
			fact = fact.multiply(BigInteger.valueOf(i));
		}
		return fact;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Thread thr = Thread.currentThread();
		BigInteger fact = calculateFactorial(n);
		System.out.println(thr.getName() + " " + n + "!=" + fact);
	}

}
