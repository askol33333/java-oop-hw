package sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Container<Integer> a = new Container<>(10);
//		Container<String> b = new Container<>("Hello world");
//
//		/// a.setElement("Java");
//
//		int number = 5 + a.getElement();
//		System.out.println("Number = " + number);
//		System.out.println(a);
//		System.out.println(b);
//
//		Integer[] array1 = new Integer[] { -5, 12, 1, -2, 15 };
//		System.out.println(getMax(array1));
//	}
//
//	public static <T extends Comparable<T>> T getMax(T[] array) {
//		T maxElement = array[0];
//		for (int i = 0; i < array.length; i++) {
//			if (maxElement.compareTo(array[i]) < 0) {
//				maxElement = array[i];
//			}
//		}
//		return maxElement;
//	}
		
		List<String> myList = new ArrayList<>();
		myList.add("Hello");
		myList.add("World");
		System.out.println(myList);
		myList.add(1, "Java");
		myList.add("Best");
		System.out.println(myList);
		String temp = myList.get(0);
		System.out.println(temp);
		Collections.sort(myList);
		System.out.println(myList);
		System.out.println();

		for (String element : myList) {
			System.out.println(element);
		}
	}
}