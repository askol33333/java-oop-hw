package sample;

import java.util.Arrays;

public class Group {
	private String groupName;
	private Student[] students = new Student[10];

	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}

	public Group() {
		super();
	}


	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void setStudents(Student[] students) {
		this.students = students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				students[i].setGroupName(groupName);
				students[i].setId(i+1);
				return;
			}
		}
		throw new GroupOverflowException("The group is full. There are no more places.");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getLastName().equals(lastName)) {
					return students[i];
				}
			}
		}
		throw new StudentNotFoundException("There is no student with the last name: "
				+ lastName.toUpperCase() + " in the group: " + groupName.toUpperCase() + ".");
	}

	public boolean removeStudentById(int id) {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String group ="Group: " + groupName.toUpperCase() +"." + System.lineSeparator();
		int count = 0; 
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				count+=1;
				group += i+1 + "." + students[i] + "." + System.lineSeparator();
			}
		}
		return group + "Total group of " + count + " students. ";
	}

}
