package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student student1 = new Student("Oleg", "Podolyan", Gender.MALE);
		Student student2 = new Student("Irina", "Belova", Gender.FEMALE);
		Student student3 = new Student("Maksim", "Spirkin", Gender.MALE);
		Student student4 = new Student("Maksim", "Nagnibeda", Gender.MALE);
		Student student5 = new Student("Olga", "Shevchenko", Gender.FEMALE);
		Student student6 = new Student("Oleksandr", "Pozhidaev", Gender.MALE);
		Student student7 = new Student("Igor", "Pruhodiko", Gender.MALE);
		Student student8 = new Student("Oleksandr", "Ugranovskiy", Gender.MALE);
		Student student9 = new Student("Mariya", "Figova", Gender.FEMALE);
		Student student10 = new Student("Volodumir", "Feschenko", Gender.MALE);
		Student student11 = new Student("Vasya", "Pyatochkin", Gender.MALE);
		
		Group group1 = new Group("JAVA-OOP-061022");
		
		try {
			
			group1.addStudent(student1);
			group1.addStudent(student2);
			group1.addStudent(student3);
			group1.addStudent(student4);
			group1.addStudent(student5);
			group1.addStudent(student6);
			group1.addStudent(student7);
			group1.addStudent(student8);
			group1.addStudent(student9);
			group1.addStudent(student10);
			
			System.out.println(group1);
			
		//	group1.addStudent(student11);
			
			System.out.println(group1.searchStudentByLastName("Shevchenko")); 
		//	System.out.println(group1.searchStudentByLastName("Dorn")); 
			
			System.out.println(group1.removeStudentById(5));
			group1.addStudent(student11);
			System.out.println();	
			System.out.println(group1);	
			
		} catch (GroupOverflowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (StudentNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}		
}	
	